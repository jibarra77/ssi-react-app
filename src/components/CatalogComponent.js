import React from 'react';
import {Breadcrumb, BreadcrumbItem, Card, CardImg, CardImgOverlay, CardTitle} from "reactstrap";
import {Link} from "react-router-dom";
import {baseUrl} from "../shared/baseUrl";

function RenderCatalogItem({item, onClick}) {
    return (
        <Link to={`/catalog/${item.id}`}>
            <Card>
                <CardImg width="100%" src={baseUrl + item.image} alt={item.name}/>
                <CardImgOverlay>
                    <CardTitle>{item.name}</CardTitle>
                </CardImgOverlay>
            </Card>
        </Link>
    );
}

const Catalog = (props) => {                      //arrow function
    let catalog = props.items.map(item => {
        return (
            <div key={item.id} className="col-12 col-md-5 m-1">
                <RenderCatalogItem item={item}/>
            </div>
        );
    });
    return (
        <div className="container">
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem active>Catalog</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>Catalog</h3>
                    <hr/>
                </div>
            </div>
            <div className="row">
                {catalog}
            </div>
        </div>
    );
};
export default Catalog;
/*let catalog = this.props.items.map(item => {                   //var ya no se usa, definir con let.
    return (
        <div key={item.id} className="col-12 col-md-5 m-1">
            <Card onClick={() => {
                this.props.onClick(item)                             //EVENTO al hacer on click llama a onitemselect y envia item
            }}>
                <CardImg width="100%" src={item.image} alt={item.name}/> {/!*contenido que se muestra por defecto*!/}
                <CardImgOverlay>
                    <CardTitle>{item.name}</CardTitle>
                </CardImgOverlay>
            </Card>
        </div>
    );
});
return (
    <div className="container">
        <div className="row">
            {catalog}
        </div>
    </div>
);*/


