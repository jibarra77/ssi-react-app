import * as ActionTypes from './ActionTypes';
import {baseUrl} from "../shared/baseUrl";

export const addComment = (itemId, rating, author, comment) => ({
    type: ActionTypes.ADD_COMMENT,
    payload: {
        itemId: itemId,
        rating: rating,
        author: author,
        comment: comment
    }
});
//es un thunk
export const fetchItems = () => (dispatch) => {
    dispatch(itemsLoading(true));
    return fetch(baseUrl + 'items')
        .then(response => response.json())
        .then(items => dispatch(addItems(items)));
};

//action creator
export const itemsLoading = () => ({
    type: ActionTypes.ITEMS_LOADING
});

//action creator
export const itemsFailed = (errmess) => ({
    type: ActionTypes.ITEMS_FAILED,
    payload: errmess
});

//action creator
export const addItems = (items) => ({
    type: ActionTypes.ADD_ITEMS,
    payload: items
});

export const fetchComments = () => (dispatch) => {
    return fetch(baseUrl + 'comments')
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errmess = new Error(error.message);
                throw errmess;
            })
        .then(response => response.json())
        .then(comments => dispatch(addComments(comments)))
        .catch(error => dispatch(commentsFailed(error.message)));
};

export const commentsFailed = (errmess) => ({
    type: ActionTypes.COMMENTS_FAILED,
    payload: errmess
});

export const addComments = (comments) => ({
    type: ActionTypes.ADD_COMMENTS,
    payload: comments
});

