export const ITEMS = [
    {
        id: 0,
        name: 'Helmet',
        image: '/assets/images/helmet.jpg',
        category: 'safety',
        label: 'ultra safe',
        price: '4.99',
        description: 'Un casco de seguridad útil para proteger en ámbitos como la construcción, fabricas y otros',
        comments: [
            {
                id: 0,
                rating: 5,
                comment: "Imagine all safety PPE in SSI! 0",
                author: "Mr T 0",
                date: "2012-10-16T17:57:28.556094Z"
            },
            {
                id: 1,
                rating: 4,
                comment: "Gives me a lot of confort, I wish I could get my mother-in-law to have it! 0",
                author: "Tania Mercado 0",
                date: "2014-09-05T17:57:28.556094Z"
            },
            {
                id: 2,
                rating: 3,
                comment: "Use it, just use it! 0",
                author: "James Ojeda 0",
                date: "2015-02-13T17:57:28.556094Z"
            },
            {
                id: 3,
                rating: 4,
                comment: "Ultimate, Reaching for the stars! 0",
                author: "Diana Cardozo 0",
                date: "2013-12-02T17:57:28.556094Z"
            },
            {
                id: 4,
                rating: 2,
                comment: "It is simply very nice! 0",
                author: "50 Cent 0",
                date: "2011-12-02T17:57:28.556094Z"
            }
        ]
    },
    {
        id: 1,
        name: 'Audio protector',
        image: '/assets/images/audio.jpg',
        category: 'safety',
        label: 'noise filter',
        price: '12.99',
        description: 'Protege de ruidos auditivos por encima de los decibeles considerados aceptables',
        comments: [
            {
                id: 0,
                rating: 5,
                comment: "Imagine all safety PPE in SSI! 1",
                author: "Mr T 1",
                date: "2012-10-16T17:57:28.556094Z"
            },
            {
                id: 1,
                rating: 5,
                comment: "Gives me a lot of confort, I wish I could get my mother-in-law to have it! 1",
                author: "Tania Barrancos 1",
                date: "2014-09-05T17:57:28.556094Z"
            },
            {
                id: 2,
                rating: 3,
                comment: "Use it, just use it! 1",
                author: "Michael Rodriguez 1",
                date: "2015-02-13T17:57:28.556094Z"
            },
            {
                id: 3,
                rating: 4,
                comment: "Ultimate, Reaching for the stars! 1",
                author: "Diana Cardozo 1",
                date: "2013-12-02T17:57:28.556094Z"
            },
            {
                id: 4,
                rating: 2,
                comment: "It is simply very nice! 1",
                author: "50 Cent 1",
                date: "2011-12-02T17:57:28.556094Z"
            }
        ]
    },
    {
        id: 2,
        name: 'Glasses',
        image: '/assets/images/glasses.jpg',
        category: 'safety',
        label: 'New technology',
        price: '31.99',
        description: 'Permite filtrar rayos ultravioleta, material anti rayas, anti caidas',
        comments: [
            {
                id: 0,
                rating: 5,
                comment: "Imagine all safety PPE in SSI! 2",
                author: "Mr T 2",
                date: "2012-10-16T17:57:28.556094Z"
            },
            {
                id: 1,
                rating: 5,
                comment: "Gives me a lot of confort, I wish I could get my mother-in-law to have it! 2",
                author: "Tania Barrancos 2",
                date: "2014-09-05T17:57:28.556094Z"
            },
            {
                id: 2,
                rating: 3,
                comment: "Use it, just use it! 2",
                author: "Michael Rodriguez 2",
                date: "2015-02-13T17:57:28.556094Z"
            },
            {
                id: 3,
                rating: 4,
                comment: "Ultimate, Reaching for the stars! 2",
                author: "Diana Cardozo 2",
                date: "2013-12-02T17:57:28.556094Z"
            },
            {
                id: 4,
                rating: 2,
                comment: "It is simply very nice! 2",
                author: "50 Cent 2",
                date: "2011-12-02T17:57:28.556094Z"
            }
        ]
    },
    {
        id: 3,
        name: 'Gloves',
        image: '/assets/images/gloves.jpg',
        category: 'safety',
        label: '',
        price: '12.99',
        description: 'Hechos de algodón con goma antideslizante, especial para industria química.',
        comments: [
            {
                id: 0,
                rating: 5,
                comment: "Imagine all safety PPE in SSI! 3",
                author: "Mr T 3",
                date: "2012-10-16T17:57:28.556094Z"
            },
            {
                id: 1,
                rating: 5,
                comment: "Gives me a lot of confort, I wish I could get my mother-in-law to have it! 3",
                author: "Tania Barrancos",
                date: "2014-09-05T17:57:28.556094Z"
            },
            {
                id: 2,
                rating: 3,
                comment: "Use it, just use it! 3",
                author: "Michael Rodriguez 3",
                date: "2015-02-13T17:57:28.556094Z"
            },
            {
                id: 3,
                rating: 4,
                comment: "Ultimate, Reaching for the stars! 3",
                author: "Diana Cardozo 3",
                date: "2013-12-02T17:57:28.556094Z"
            },
            {
                id: 4,
                rating: 2,
                comment: "It is simply very nice! 3",
                author: "50 Cent 3",
                date: "2011-12-02T17:57:28.556094Z"
            }
        ]
    }
];